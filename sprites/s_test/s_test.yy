{
    "id": "240cf972-2856-434c-bf79-5db6d9c59ad3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d6476cd9-b8a1-4c8f-8cbb-11b3f451a4ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "240cf972-2856-434c-bf79-5db6d9c59ad3",
            "compositeImage": {
                "id": "c30f3473-2b66-4aec-9898-8bbb512a741c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6476cd9-b8a1-4c8f-8cbb-11b3f451a4ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb08ec9-53d5-44e2-b27f-979ca5971dfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6476cd9-b8a1-4c8f-8cbb-11b3f451a4ee",
                    "LayerId": "c44088ca-9936-42f3-857c-c0013f167311"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c44088ca-9936-42f3-857c-c0013f167311",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "240cf972-2856-434c-bf79-5db6d9c59ad3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}