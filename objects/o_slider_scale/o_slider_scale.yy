{
    "id": "376d1213-811d-49d6-8278-7c03a41a86b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slider_scale",
    "eventList": [
        {
            "id": "217be001-d8cc-4738-92b2-928c664736a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "376d1213-811d-49d6-8278-7c03a41a86b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "3e8f26a0-66e4-476e-a703-5583aa9da247",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0398f642-130b-4dd4-a892-e787cd0d748d",
    "visible": true
}