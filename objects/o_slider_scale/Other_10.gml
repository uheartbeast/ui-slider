/// @description Slider event
if instance_exists(o_test) {
	o_test.image_xscale = 1+value_;
	o_test.image_yscale = 1+value_;
}
