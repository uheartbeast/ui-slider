{
    "id": "3e8f26a0-66e4-476e-a703-5583aa9da247",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slider_parent",
    "eventList": [
        {
            "id": "19bce5d4-3ca0-4649-b35a-e70cb2e65a3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e8f26a0-66e4-476e-a703-5583aa9da247"
        },
        {
            "id": "222ef53e-ad58-488c-adab-ebb7e2a6cc1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e8f26a0-66e4-476e-a703-5583aa9da247"
        },
        {
            "id": "2fff8e83-ae7e-4de7-ab2c-f9aa9bc3a651",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3e8f26a0-66e4-476e-a703-5583aa9da247"
        },
        {
            "id": "df927a26-f021-4e8b-8cb7-d091445d89e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3e8f26a0-66e4-476e-a703-5583aa9da247"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0398f642-130b-4dd4-a892-e787cd0d748d",
    "visible": true
}