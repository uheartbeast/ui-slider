{
    "id": "fd9c3b2a-e7fa-4864-b98b-bc9747ef2cc1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slider_x",
    "eventList": [
        {
            "id": "e1b6bec7-8411-4052-9905-1a9f135df96f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "fd9c3b2a-e7fa-4864-b98b-bc9747ef2cc1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "3e8f26a0-66e4-476e-a703-5583aa9da247",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0398f642-130b-4dd4-a892-e787cd0d748d",
    "visible": true
}